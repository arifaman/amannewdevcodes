﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericCollectionDemoApp
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> lst = new List<string>(10);

            lst.Add("S1");
            lst.Add("S3");
            lst.Add("S2");
            lst.Add("S0");
            lst.AddRange(new string[] { "S10", "S6", "S9" });
            lst.Insert(2, "Special");
            lst.Remove("S2");
            lst.Sort();
            lst.TrimExcess();
            foreach(string str in lst)
                Console.WriteLine(str);
            Console.WriteLine("Capacity is {0} and Size is {1}",lst.Capacity,lst.Count);
            lst.ToArray();
            Console.WriteLine(lst[2]);
        }
    }
}
