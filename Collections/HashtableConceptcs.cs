﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class HashtableConceptcs
    {
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();

            ht.Add(100, "User1");
            ht.Add(105, "User2");
            ht.Add(102, "User3");
            ht.Add(109, "User5");
            ht.Add(101, "User8");

            ht.Remove(102);

            //ht.Clear();

            Console.WriteLine(ht[101].ToString());

            //print all elements

            foreach (DictionaryEntry de in ht)
            {
                int key = (int)de.Key;
                string value = de.Value.ToString();
                Console.WriteLine(key + "----" + value);
            }
        }
    }
}
