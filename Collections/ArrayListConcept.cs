﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class ArrayListConcept
    {
        static void Main(string[] args)
        {
            ArrayList al = new ArrayList();

            //add element

            al.Add("Aman");
            al.Add("Testing");
            al.Add("Automation");
            al.Add("Test");
            al.Add("A5");

            //adding range

            al.AddRange(new string[] {"A1","A2","A3","A4" });

            //insert element

            al.Insert(3,"New value");
            Console.WriteLine(al[3]);

            //Remove

            //al.Remove("Testing");
           // al.RemoveAt(1);
            Console.WriteLine(al[1]);

            Console.WriteLine("----------------------------");

            for(int i = 0;i<al.Count;i++)
                Console.WriteLine(al[i]);

            Console.WriteLine("--------------------");
            foreach(string ele in al)
                Console.WriteLine(ele.ToString());

            Console.WriteLine("-------------------------");

            al.Sort();
         //   al.Reverse();

            foreach (string ele in al)
                Console.WriteLine(ele.ToString());
            al.TrimToSize();
            
            Console.WriteLine("Capacity is : {0} and size is {1}", al.Capacity,al.Count);
            
            //convert ArrayList to Array
            Console.WriteLine("----------------------");

             string[] str = (string[])al.ToArray(typeof(string));
            
            foreach(string data in str)
                Console.WriteLine(data);

        }
    }
}
