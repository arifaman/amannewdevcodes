﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class Employee/* : IComparable*/
    {
        public int id;
        public string name;

        //public int CompareTo(object obj)
        //{
        //    Employee e2 = (Employee)obj;
        //    if (this.id > e2.id)
        //        return 1;
        //    else if (this.id < e2.id)
        //        return -1;
        //    else
        //        return 0;
        //}

        public override string ToString()
        {
            return id + "--" + name;
        }
    }

    public class EmployeeCompare : IComparer
    {
        public int Compare(object x, object y)
        {
            Employee firstEmployee = (Employee)x;
            Employee secondEmployee = (Employee)y;
            return firstEmployee.id - secondEmployee.id;
        }
    }

    class MoreCollections
    {
        static void Main(string[] args)
        {
            ArrayList alist = new ArrayList();
            alist.Add(new Employee() { id = 30, name = "Emp1" });
            alist.Add(new Employee() { id = 00, name = "Emp2" });
            alist.Add(new Employee() { id = 10, name = "Emp3" });
            
            foreach(Employee emp in alist)
                Console.WriteLine(emp.ToString());

            //Code behind the screen

            IEnumerator en = alist.GetEnumerator();
            while (en.MoveNext())
            {
                Employee e = (Employee)en.Current;
                Console.WriteLine(e);
            }
            Console.WriteLine("---------------");

            //alist.Sort();
            foreach (Employee emp in alist)
                Console.WriteLine(emp.ToString());
            Console.WriteLine("-------------");

            alist.Reverse();
            foreach (Employee emp in alist)
                Console.WriteLine(emp.ToString());

            Console.WriteLine("------------------");

            EmployeeCompare ec = new EmployeeCompare();
            alist.Sort(ec);
            foreach (Employee e in alist)
                Console.WriteLine(e.ToString());
        }
    }
}
