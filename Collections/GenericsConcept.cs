﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections1
{
    class GenericsConcept
    {
        static void Main(string[] args)
        {
            Stack<double> st1 = new Stack<double>(2);
            Stack<double> st2 = new Stack<double>(2);
            Stack<int> st3 = new Stack<int>(2);
            Stack<int> st4 = new Stack<int>(2);

            Console.WriteLine(st1.GetType(). GetHashCode());
            Console.WriteLine(st2.GetType().GetHashCode());
            Console.WriteLine(st3.GetType().GetHashCode());
            Console.WriteLine(st4.GetType().GetHashCode());
            //st.Push(10);
            //st.Push(2);
            //st.Print();
            ////double result = (double)st.Pop(); // boxing-Unboxing everytime
            //double result = st.Pop();
        }
    }

    class Stack<T> where T : struct
    {
        T[] data;
        int top = -1;
        public Stack(int size)
        {
            data = new T[size];
        }

        public void Push(T value)
        {
            top++;
            data[top] = value;
        }

        public T Pop()
        {
            T value = data[top];
            top--;
            return value;
        }

        public T GetTopElement()
        {
            return data[top];
        }

        public void Print()
        {
            for (int i = 0; i < data.Length; i++)
                Console.WriteLine(data[i]);
        }
    }
}
