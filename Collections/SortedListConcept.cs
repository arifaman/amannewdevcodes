﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class SortedListConcept
    {
        static void Main(string[] args)
        {
              SortedList sl = new SortedList();

            sl.Add("t1", "User1");
            sl.Add("t5", "User2");
            sl.Add("t9", "User3");
            sl.Add("t0", "User5");
            sl.Add("t4", "User8");

            sl.Remove("t1");

            //ht.Clear();

            Console.WriteLine(sl["t9"]);

            //print all elements

            foreach (DictionaryEntry de in sl)
            {
                string key = de.Key.ToString();
                string value = de.Value.ToString();
                Console.WriteLine(key + "----" + value);
            }
        }
    }
}
