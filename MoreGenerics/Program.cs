﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoreGenerics
{
    class Program
    {
        static void Main(string[] args)
        {
            SwapValues<int>(12, 20);
        }
        public static void SwapValues<T>(T a, T b)
        {
            Console.WriteLine("Before swap : {0} and {1}", a, b);
            T temp;
            temp = a;
            a = b;
            b = temp;
            Console.WriteLine("After swap : {0} and {1}", a, b);
        }
    }
}
