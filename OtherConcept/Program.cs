﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OtherConcept
{
    class Program
    {
        struct fileParameters
        {
            public string fileName;
            public string filePart1;
            public string filePart2;
            public string filePart3;
        }
        static void Main(string[] args)
        {
            fileParameters fileParam;
            fileParam.filePart1 = "PART1";
            fileParam.filePart2 = "PART2";
            fileParam.filePart3 = "TESTING";

            fileParam.fileName = string.Concat(fileParam.filePart1, fileParam.filePart2);
            Console.WriteLine(fileParam.fileName);
        }
    }
}
