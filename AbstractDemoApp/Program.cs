﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractDemoApp
{
    abstract class Figure
    {
        protected int Dimention;
        public Figure(int dimention)
        {
            Dimention = dimention;
        }
        
        public string shape = null;
        public abstract double Area();
        public abstract double Perimeter();

    }

    class Circle : Figure
    {
        public Circle(int radius): base(radius)
        {
        }

        public override double Area()
        {
            return Math.PI * Dimention * Dimention;
        }

        public override double Perimeter()
        {
            return 2 * Math.PI * Dimention;
        }
    }

    class Square : Figure
    {
        public Square(int sides) : base(sides)
        {
        }
        public override double Area()
        {
            return Dimention * Dimention;
        }

        public override double Perimeter()
        {
            return 4 * Dimention;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Figure fig;
            if (args[0] == "C")
            {
                fig = new Circle(20);
                fig.shape = "Circle";
            }
            else
            {
                fig = new Square(20);
                fig.shape = "Square";
            }
            Console.WriteLine("Area of {0} is {1}  ", fig.shape,fig.Area());
            Console.WriteLine("Perimeter of {0} is {1}", fig.shape, fig.Perimeter());
        }
    }
}
