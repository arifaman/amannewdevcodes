﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling
{
    class WritingFiles
    {
        static void Main(string[] args)
        {
            FileStream fs = null;
            StreamWriter sw = null;
            try
            {
                try
                {
                    fs = new FileStream(@"D:\C# Files\Test.txt", FileMode.CreateNew, FileAccess.Write, FileShare.None);
                }
                catch (IOException)
                {
                    Console.WriteLine("File is already existing.Do you want to overwrite?(Y/N)");
                    ConsoleKeyInfo key = Console.ReadKey(true);
                    if (key.KeyChar == 'Y' || key.KeyChar == 'y')
                    {
                        Console.WriteLine("Ready to overwrite the file.Please enter the text");
                        fs = new FileStream(@"D:\C# Files\Test.txt", FileMode.Truncate, FileAccess.Write, FileShare.None);
                    }
                    else
                    {
                        Console.WriteLine("Closing the process.Please wait...");
                        return;
                    }
                }
                sw = new StreamWriter(fs);
                while (true)
                {
                    int data = Console.Read();
                    if (data == -1)
                        break;
                    sw.Write((char)data);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (sw != null)
                    sw.Close();
            }
        }
    }
}
