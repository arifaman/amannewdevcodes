﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling
{
    class ReadingFiles
    {
        static void Main(string[] args)
        {
            FileStream fs = null;
            StreamReader sr = null;
            try
            {
                fs = new FileStream(@"D:\C# Files\Test.txt", FileMode.OpenOrCreate, FileAccess.Read, FileShare.None);
                sr = new StreamReader(fs,System.Text.Encoding.ASCII);
                while (true)
                {
                    int data = sr.Read();
                    if (data == -1)
                        break;
                    Console.Write((char)data);
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                if (fs != null)
                    fs.Close();
            }


        }
    }
}
