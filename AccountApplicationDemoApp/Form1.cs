﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AccountApplicationDemoApp
{
    public partial class Form1 : Form
    {
        Account a;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            a = new Account();
            //Account a1 = new Account(99,"Washi",5000);
            //Account a2 = new Account(a1);
        }

        private void btnSet_Click(object sender, EventArgs e)
        {   
            a.Name = txtName.Text;
            a.Deposit(decimal.Parse(txtBalance.Text));
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            txtId.Text = a.Id.ToString();
            txtName.Text = a.Name;
            txtBalance.Text = a.Balance.ToString();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtId.Clear();
            txtName.Clear();
            txtBalance.Clear();
        }

        private void btnTemp_Click(object sender, EventArgs e)
        {
            Account a1 = new Account();
            a = a1;
            a1 = null;
        }

        private void btnGetGeneration_Click(object sender, EventArgs e)
        {
            MessageBox.Show(GC.GetGeneration(a).ToString());
        }

        private void btnDestroy_Click(object sender, EventArgs e)
        {
            a = null;
        }

        private void btnGC_Click(object sender, EventArgs e)
        {
            GC.Collect();
        }

        private void btnDeposit_Click(object sender, EventArgs e)
        {
            a.Deposit(decimal.Parse(txtAmount.Text));
        }

        private void textWithdraw_Click(object sender, EventArgs e)
        {
            a.Withdrawal(decimal.Parse(txtAmount.Text));
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void btnGetMB_Click(object sender, EventArgs e)
        {
            txtMB.Text = Account.MinBalance.ToString();
        }

        private void btnSetMB_Click(object sender, EventArgs e)
        {
            Account.MinBalance = int.Parse(txtMB.Text);
        }

        private void txtMB_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
