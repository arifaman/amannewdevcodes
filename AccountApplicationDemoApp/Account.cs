﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AccountApplicationDemoApp
{
    class Account
    {
        private static int _Id;
        private string _Name;
        private decimal _Balance;
        //public bool isIdAlreadySet;
        private static int _MinBalance = 500;
        private static int _PrevId;

        static Account()
        {
            MessageBox.Show("Class is loaded into the memory");
        }
        public Account()
        {
            MessageBox.Show("Blank constructor is executing now");
            _PrevId++;
            _Id = _PrevId;
        }

        public Account(string name, decimal balance) : this()
        {
            MessageBox.Show("Parametered constructor is executing now");
            this.Name = name;
            this.Balance = balance;
        }

        public Account(Account a) : this(a._Name, a._Balance)
        {
            MessageBox.Show("Executing copy constructor now");
            //this.Id = a.Id;
            this.Name = a.Name;
            this.Balance = a.Balance;
        }

        public decimal Balance
        {
            get
            {
                return _Balance;
            }
            private set
            {
                _Balance = value;
            }
        }

        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (value.Length > 18)
                    throw new ApplicationException("Name can not be more than 18 chars");
                _Name = value;
            }
        }

        public int Id
        {
            get
            {
                return _Id;
            }
            //private set
            //{
            //    if (isIdAlreadySet)
            //        throw new ApplicationException("Id can not be set multiple times");
            //    _Id = value;
            //    isIdAlreadySet = true;
            //}
        }

        public static int MinBalance
        {
            get
            {
                return _MinBalance;
            }
            set
            {
                if (value < 400 || value > 1000)
                {
                    throw new ApplicationException("MinBalance should be between 400 - 1000");
                }
                else
                {
                    _MinBalance = value;
                }
            }
        }

        public string SampleData { get; set; }


        public void Deposit(decimal amount)
        {
            this._Balance += amount;
        }
        public void Withdrawal(decimal amount)
        {
            if (this._Balance - amount < MinBalance)
                throw new ApplicationException("Remaining amount is less than min balance");
            else
                this._Balance -= amount;
        }
    }
}
