﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllConceptsOnCollections
{
    class HashtableConcept
    {
        static void Main(string[] args)
        {
            Hashtable hTable = new Hashtable();
            string[] arr = new string[4];

            hTable.Add(101, "Testing1");
            hTable.Add(110, "Testing110");
            hTable.Add(105, "Testing105");
            hTable.Add(104, "Testing104");

            Console.WriteLine(hTable.ContainsKey(115));
            Console.WriteLine(hTable.ContainsValue("Testing155"));

            foreach (DictionaryEntry dEntry in hTable)
            {
                Console.WriteLine(dEntry.Key + "----" + dEntry.Value);
            }

        }
    }
}
