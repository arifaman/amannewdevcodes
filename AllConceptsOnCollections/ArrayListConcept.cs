﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllConceptsOnCollections
{
    class ArrayListConcept
    {
        static void Main(string[] args)
        {
            ArrayList aList = new ArrayList();

            aList.Add("Testing1");
            aList.Add("Testing9");
            aList.Add("Testing3");
            aList.Add("Testing8");
            aList.Add("Testing2");

            aList.AddRange(new string[] { "Testing18", "Testing11", "Testing15","Testing10" });

            aList.Remove("Testing3");
            aList.RemoveAt(2);

            aList.TrimToSize();
            Console.WriteLine("Capacity is {0} and size is : {1}", aList.Capacity, aList.Count);

            aList.Sort();
            //print all

            foreach (string str in aList)
            {
                Console.WriteLine(str);
            }

            Console.WriteLine("-------------CovertingToArray--------------");

            string[] myArray = (string[])aList.ToArray(typeof(string));

            foreach (string data in myArray)
            {
                Console.WriteLine(data);
            }



        }
    }
}
