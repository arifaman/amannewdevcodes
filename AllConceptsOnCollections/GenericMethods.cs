﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllConceptsOnCollections
{
    class GenericMethods
    {
        static void Main(string[] args)
        {
            SwapValues<string>("Arif", "Aman");
        }

        public static void SwapValues<T>(T a, T b)
        {
            Console.WriteLine("Before swap First is {0} and Second is {1}",a,b);
            T temp;
            temp = a;
            a = b;
            b = temp;
            Console.WriteLine("After swap First is {0} and Second is {1}", a, b);
        }


    }
}
