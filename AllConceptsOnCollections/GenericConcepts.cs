﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllConceptsOnCollections
{

    class Stack<T> where T : struct, IComparable
    {
        T[] data;
        int top = -1;

        public Stack(int size)
        {
            data = new T[size];
        }

        public void Push(T value)
        {
            top++;
            data[top] = value;
        }

        public T Pop()
        {
            T value = data[top];
            top--;
            return value;
        }

        public T GetTopElement()
        {
            return data[top];
        }

        public void Print()
        {
            for(int i = 0;i<=top;i++)
                Console.WriteLine(data[i]);
        }
    }
    
    class GenericConcepts
    {
        static void Main(string[] args)
        {
            //Integer
            //Stack<int> s = new Stack<int>(5);

            //s.Push(10);
            //s.Push(5);
            //s.Push(34);

            //int ele =s.Pop(); // always the top element
            //Console.WriteLine(ele);
            //Console.WriteLine("------------Print All");

            //s.Print();

            //Double

            Stack<double> s = new Stack<double>(5);

            s.Push(100.12);
            s.Push(5.69);
            s.Push(34.99);

            double ele = s.Pop(); // always the top element
            Console.WriteLine(ele);
            Console.WriteLine("------------Print All");

            s.Print();

        }
    }
}
