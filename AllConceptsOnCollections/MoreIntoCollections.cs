﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllConceptsOnCollections
{
    class Employee //: IComparable
    {
        public int empId;
        public string empName;

        public int CompareTo(object obj)
        {
            Employee otherClass = (Employee)obj;

            if (this.empId < otherClass.empId)
                return -1;
            else if (this.empId > otherClass.empId)
                return 1;
            else
                return 0;
        }

        public override string ToString()
        {
            return empId + "--" + empName;
        }
    }

    public class EmployeeSorting : IComparer
    {
        public int Compare(object x, object y)
        {
            Employee empClass1 = (Employee)x;
            Employee empClass2 = (Employee)y;

            if (empClass1.empId < empClass2.empId)
                return -1;
            else if (empClass1.empId > empClass2.empId)
                return 1;
            return 0;
        }
    }

    class MoreIntoCollections
    {
        static void Main(string[] args)
        {
            ArrayList alist = new ArrayList();

            alist.Add(new Employee() { empId = 0001, empName = "Arif" });
            alist.Add(new Employee() { empId = 0010, empName = "Aman" });
            alist.Add(new Employee() { empId = 0005, empName = "John" });
            alist.Add(new Employee() { empId = 0020, empName = "Parker" });

            foreach (Employee emp in alist)
            {
                Console.WriteLine(emp.ToString());
            }

            //sorting
            Console.WriteLine("Sorting now...");

            //alist.Sort();
            EmployeeSorting es = new EmployeeSorting();
            alist.Sort(es);
            foreach (Employee emp1 in alist)
            {
                Console.WriteLine(emp1.ToString());
            }
        }
    }
}
