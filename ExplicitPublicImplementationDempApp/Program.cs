﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplicitPublicImplementationDempApp
{
    interface IA1
    {
        void Foo1();
        void Foo2();
    }

    interface IA2
    {
        void Foo2();
        void Foo3();
    }

    class MyClass : IA1, IA2
    {
        public void Foo1()
        {
            Console.WriteLine("Foo1 from MyClass for IA1");
        }
        void IA1.Foo2()
        {
            Console.WriteLine("Foo2 from MyClass for A1 only");
        }

        void IA2.Foo2()
        {
            Console.WriteLine("Foo2 from MyClass for A1 only");
        }

        public void Foo3()
        {
            Console.WriteLine("Foo3 from MyClass for IA2");
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            IA1 a1;
            IA2 a2;
            MyClass c;

            a1 = new MyClass();
            a2 = new MyClass();
            //c = new MyClass();

            //a1.Foo2();
            //a2.Foo2();
            //((IA1)c).Foo2(); //c is very big

            c = new MyClass();
            a1 = c; // implicit interface casting

            c = (MyClass)a1; //Explicit interface casting required

            a1 = (IA1)a2;

            a2 = (IA2)a1;




        }
    }
}
