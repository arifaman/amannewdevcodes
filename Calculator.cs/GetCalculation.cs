﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class GetCalculation
    {
        public double num1;
        public double num2;
        public double result = 0.00;

        public double Add()
        {
            return result = num1 + num2;
        }

        public double Sub()
        {
            return result = num1 - num2;
        }
    }
}
