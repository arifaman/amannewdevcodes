﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemDemoApp
{
    class UsingFile
    {
        static void Main(string[] args)
        {
            if (File.Exists(@"D:\C# Files\Test.txt"))
                File.Delete(@"D:\C# Files\Test.txt");
            StreamWriter sw = File.CreateText(@"D:\C# Files\Test.txt");
            sw.WriteLine("Writing from code 1");
            sw.WriteLine("Writing from code 2");
            sw.WriteLine("Writing from code 3");
            sw.Close();
        }
    }

    class usingFileInfo
    {
        static void Main(string[] args)
        {
            FileInfo fi = new FileInfo(@"D:\C# Files\Test.txt");
            if (fi.Exists)
                fi.Delete();
            StreamWriter sw = fi.CreateText();
            sw.WriteLine("This is our Fileino Code 1");
            sw.WriteLine("This is our Fileino Code 2");
            sw.WriteLine("This is our Fileino Code 3");
            sw.WriteLine("This is our Fileino Code 4");
            sw.Close();
        }
    }

    class UsingDirectoryInfo
    {
        static void Main(string[] args)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(args[0]);
            if (!dirInfo.Exists)
            {
                Console.WriteLine("Directory does not exist");
                return;
            }

            FileInfo[] allFiles = dirInfo.GetFiles("*.txt");
            foreach (FileInfo file in allFiles)
            {
                Console.WriteLine(file.Name + "--" + file.Length + "--" + file.IsReadOnly);
            }

            Console.WriteLine("---------------------------------------");

            DirectoryInfo[] allSubDirectories = dirInfo.GetDirectories();
            foreach (DirectoryInfo dir in allSubDirectories)
            {
                Console.WriteLine(dir.Name);
            }
        }
    }

    class OtherFileFunctions
    {
        static void Main(string[] args)
        {
            File.Move(@"D:\C# Files\Test.txt", @"D:\C# Files\Test1.txt");
            File.Copy(@"D:\C# Files\Test1.txt", @"D:\C# Files\Test2.txt");
        }
    }
}
