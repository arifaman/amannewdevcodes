﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionHandlingConcept
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int n1 = int.Parse(args[0]);
                int n2 = int.Parse(args[1]);

                int result = DivNumbers(n1, n2);
                Console.WriteLine("Result is  : " + result);
            }

            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Make sure you have proper arguments for the calculations");
            }

            catch (DivideByZeroException e2)
            {
                Console.WriteLine("Please do not divide by 0");
                Console.WriteLine(e2.Message);
            }

            catch (FormatException)
            {
                Console.WriteLine("The format of the arguments are not correct");
            }
            catch (Exception)
            {
                Console.WriteLine("Error");
            }
        }

        public static int DivNumbers(int a, int b)
        {
            try
            {
                return a / b;
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Please enter a non-zero number as second number");
                return int.MinValue;
            }
        }
    }
}
