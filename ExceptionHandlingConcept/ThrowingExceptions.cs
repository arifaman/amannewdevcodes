﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionHandlingConcept
{
    class ThrowingExceptions
    {
        static void Main(string[] args)
        {
            try
            {
                Foo();
            }
            catch (ApplicationException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.InnerException.Message);
            }
        }

        static void Foo()
        {
            try
            {
                Bar();
            }
            catch (DemoException ex)
            {
                //Codes for loggs
                //throw; // preserving stack
                //throw ex; //Resetting the stack
                throw new ApplicationException("Application Exception....",ex);
            }
        }

        static void Bar()
        {
            throw new DemoException("Demo Exception...",10);
        }
    }
}
