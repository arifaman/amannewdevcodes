﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionHandlingConcept
{
    class FinallyBlock
    {
        static void Main(string[] args)
        {
            try
            {
                Foo();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static void Foo()
        {
            //FileStream fs = null; ;
            //try
            //{
            //    fs = new FileStream("D:\\test.txt", FileMode.OpenOrCreate);
            //    throw new ApplicationException("Some error");
            //}
            //finally
            //{
            //    if (fs != null)
            //        fs.Close();
            //}

            using (FileStream fs = new FileStream("D:\\test.txt", FileMode.OpenOrCreate))
            {
                throw new ApplicationException("Some error");
            }
        }


    


    }
}
