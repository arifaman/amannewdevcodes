﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SerilizationDemoApp
{
    [Serializable]
    public class Point
    {
        public int X, Y;
    }

    [Serializable]
    public class Line
    {
        public Point P1, P2;
    }

    class BinarySerializing
    {
        static void Main(string[] args)
        {
            Line ln = new Line();
            ln.P1 = new Point() { X = 10, Y = 11 };
            ln.P2 = new Point() { X = 20, Y = 21 };

            FileStream fs = new FileStream(@"D:\C# Files\Sample.dat", FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, ln);
            fs.Close();
        }
    }

    class BinaryDeserializing
    {
        static void Main(string[] args)
        {
            FileStream fs = new FileStream(@"D:\C# Files\Sample.dat", FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            Line line = (Line)bf.Deserialize(fs);
            Console.WriteLine(line.P1.X + "--" + line.P1.Y);
            Console.WriteLine(line.P2.X + "--" + line.P2.Y);
        }
    }

    //XMLSerialization
    class XmlSerializing
    {
        static void Main(string[] args)
        {
            Line ln = new Line();
            ln.P1 = new Point() { X = 10, Y = 11 };
            ln.P2 = new Point() { X = 20, Y = 21 };

            FileStream fs = new FileStream(@"D:\C# Files\SampleFile.xml", FileMode.Create);
            XmlSerializer xs = new XmlSerializer(typeof(Line));
            xs.Serialize(fs, ln);
            fs.Close();
        }
    }

    class XmlDeserializing
    {
        static void Main(string[] args)
        {
            FileStream fs = new FileStream(@"D:\C# Files\SampleFile.xml", FileMode.Open);
            XmlSerializer xs = new XmlSerializer(typeof(Line));
            Line line = (Line)xs.Deserialize(fs);
            Console.WriteLine(line.P1.X + "--" + line.P1.Y);
            Console.WriteLine(line.P2.X + "--" + line.P2.Y);
        }
    }


}
