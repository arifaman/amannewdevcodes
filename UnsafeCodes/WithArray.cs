﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnsafeCodes
{
    class WithArray
    {
        static unsafe void PrintArray(int* par, int len)
        {
            for (int i = 0; i < len; i++)
            {
                Console.WriteLine(*(par+i));
            }
        }

        static void Main(string[] args)
        {
            int[] arr = { 1, 2, 3, 4 };
            unsafe
            {
                fixed (int* par = arr)
                {
                    PrintArray(par,arr.Length);
                }
            }
        }
    }
}
