﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnsafeCodes
{
    class WithStruct
    {
        struct Demo
        {
            public int M, N;
        }

        static unsafe void PrintDemo(Demo* pDemo)
        {
            Console.WriteLine((*pDemo).M + "--" + (*pDemo).N);
        }
            
        static void Main(string[] args)
        {
            Demo d;
            d.M = 10;
            d.N = 20;
            unsafe
            {
                PrintDemo(&d);
            }
        }
    }
}
