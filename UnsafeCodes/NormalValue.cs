﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnsafeCodes
{
    class NormalValue
    {
        static int Square(int n)
        {
            return n * n;
        }

        static unsafe void Square(int* pn)
        {
            *pn = (*pn) * (*pn); //value at the location
        }
        static void Main(string[] args)
        {
            int n = 10;
            unsafe
            {
                Square(&n); //address of n
            }
            Console.WriteLine(n);
        }
    }
}
