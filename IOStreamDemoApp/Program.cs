﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOStreamDemoApp
{
    class ReadNoParamConcept
    {
        static void Main(string[] args)
        {
            int data = Console.Read();
            Console.WriteLine((char)data);
        }
    }

    class ReadLineConcept
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter your name");
            string name = Console.ReadLine();
            Console.WriteLine("Hello {0}, welcome to Melbourne",name);
        }
    }

    class ChangeDefaultIOSystemReader
    {
        static void Main(string[] args)
        {
            TextReader tr = new StringReader("This is line 1\nThis is line 2\n");
            Console.SetIn(tr);
            string line1 = Console.ReadLine();
            string line2 = Console.ReadLine();
        }
    }

    class ChangeDefaultIOSystemWriter
    {
        static void Main(string[] args)
        {
            TextWriter tw = new StringWriter();
            Console.SetOut(tw);
            Console.WriteLine("This is 1");
            Console.WriteLine("This is 2");

            string str = tw.ToString();
        }
     
    }


}
