﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllFilesCodes
{
    class WritingIntoFiles
    {
        static void Main(string[] args)
        {
            FileStream fs = null;
            StreamWriter sw = null;
            string keyFlag = null;
            
            try
            {
                try
                {
                    fs = new FileStream(@"D:\C# Files\DemoTest.txt", FileMode.CreateNew, FileAccess.Write, FileShare.None);
                }
                catch (IOException)
                {
                    Console.WriteLine("File is already existing. Do you want to overwrite(Y/N)?");
                    ConsoleKeyInfo key = Console.ReadKey(true);
                    if (key.KeyChar.ToString().ToUpper().Equals("Y"))
                    {
                        keyFlag = "Y";
                        Console.WriteLine("Preparing the file to be overwritten now. Please wait..");
                        fs = new FileStream(@"D:\C# Files\DemoTest.txt", FileMode.Truncate, FileAccess.Write, FileShare.None);
                    }
                    else
                    {
                        keyFlag = "N";
                        Console.WriteLine("Cancelling the process");
                        return;
                    }
                }
                //writing into the file now
                sw = new StreamWriter(fs);
                Console.WriteLine("Enter the content to be written");
                while (true)
                {  
                    int data =  Console.Read();
                    if (data == -1)
                    {
                        Console.WriteLine("Content entering process is completed");
                        break;
                    }
                    sw.Write((char)data);
                }
            }
            finally
            {
                if(keyFlag.Equals("Y"))
                Console.WriteLine("File has been prepared. Please check");
                if (sw != null)
                    sw.Close();
            }
        }
    }
}
