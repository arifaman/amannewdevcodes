﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllFilesCodes
{
    class ReadingFile
    {
        static void Main(string[] args)
        {
            FileStream fs = null;
            StreamReader sr = null;

            try
            {
                fs = new FileStream(@"D:\C# Files\DemoTest.txt",FileMode.Open,FileAccess.Read,FileShare.None);
                sr = new StreamReader(fs);
                string readAll = sr.ReadToEnd();
                Console.WriteLine(readAll);
            }
            catch (FileNotFoundException f)
            {
                Console.WriteLine(f.Message);
            }
            finally
            {
                sr.Close();
            }
        }
    }
}
