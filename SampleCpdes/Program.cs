﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleCpdes
{
    class Parent
    {
        public int PubA;
        private int PriA;
        protected int ProA;

        public Parent()
        {
            Console.WriteLine("From blank Constructor");
        }

        public Parent(int pubA, int priA, int proA)
        {
            PubA = pubA;
            PriA = priA;
            ProA = proA;
        }

    }

    class Child : Parent
    {
        public int PubB;

        public Child()
        {

        }

        public Child(int pubA, int priA, int proA, int pubB)
            : base(pubA,priA,proA)
        {
            PubB = pubB;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            //Parent p ;
            //Child c;

            //p = new Child();
            ////c = new Child();
            //////c = new Parent(); //not possible

            ////p = c; // implicit casting
            ////c = (Child)p; // explicit casting required

            //c = p as Child;

            //if (p is Child)
            //    Console.WriteLine("p is Child now");
            //else
            //    Console.WriteLine("p is not Child now");



            Parent p1 = new Parent();
            Parent p2 = null;

            Parent p = p2 ?? p1;
            Console.WriteLine(p);
            
        }
    }
}
