﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileStreamCodes
{
    class Reading1Byte
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a character");
            while (true)
            {
                int data = Console.Read();
                if (data == -1)
                    break;
                Console.WriteLine((char)data);
            }
        }


          
    }
}
