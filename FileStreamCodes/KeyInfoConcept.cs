﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileStreamCodes
{
    class KeyInfoConcept
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Do you want to continue?(Y/N)");
            ConsoleKeyInfo key = Console.ReadKey(true);
            if (key.KeyChar == 'y' || key.KeyChar == 'Y')
            {
                Console.WriteLine("You have agreed to proceed further");
            }
            else
            {
                Console.WriteLine("Exiting the process now.. Please wait");
            }
        }
    }
}
