﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileStreamCodes
{
    class StreamReaderConcept
    {
        static void Main(string[] args)
        {
            FileStream fs = new FileStream(@"D:\C# Files\Test.txt", FileMode.OpenOrCreate, FileAccess.Read, FileShare.None);
            StreamReader sr = new StreamReader(fs);
            while (true)
            {
                int data = sr.Read();
                if (data == -1)
                    break;
                Console.WriteLine((char)data);
            }

        }
    }
}
