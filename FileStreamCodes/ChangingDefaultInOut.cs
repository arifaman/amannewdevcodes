﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileStreamCodes
{
    class ChangingDefaultIn
    {
        static void Main(string[] args)
        {
            TextReader tr = new StringReader("This is line 1 \n This is line 2");
            Console.SetIn(tr);
            string line1 = Console.ReadLine();
            string line2 = Console.ReadLine();
        }
    }

    class ChangingDefaultOut
    {
        static void Main(string[] args)
        {
            TextWriter tw = new StringWriter();
            Console.SetOut(tw);
            Console.WriteLine("This is 1");
            Console.WriteLine("This is 2");
            string str = tw.ToString();
        }
    }
}
