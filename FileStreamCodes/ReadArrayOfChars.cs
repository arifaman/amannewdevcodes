﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileStreamCodes
{
    class ReadArrayOfChars
    {
        static void Main(string[] args)
        {
            char[] buffer = new char[5];

            int data = Console.In.Read(buffer, 0, 5);

            for (int i = 0; i < buffer.Length; i++)
            {
                Console.WriteLine((int)buffer[i]);
            }
        }
    }
}
