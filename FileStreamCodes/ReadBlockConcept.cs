﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileStreamCodes
{
    class ReadBlockConcept
    {
        static void Main(string[] args)
        {
            char[] buffer = new char[5];

            int data = Console.In.ReadBlock(buffer, 0, 5);

            for (int i = 0; i < buffer.Length; i++)
            {
                Console.WriteLine((int)buffer[i]);
            }
        }
    }
}
