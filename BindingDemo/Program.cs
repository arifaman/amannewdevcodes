﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BindingDemo
{
    class Parent
    {
        public void Foo()
        {
        }

        public virtual void Foo(int a)
        {
        }
    }

    class Child : Parent
    {
        public new void Foo()
        {
        }

        public override void Foo(int a)
        {
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
