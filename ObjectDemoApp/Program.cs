﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectDemoApp
{
    class Point
    {
        public int X;
        public int Y;

        public override string ToString()
        {
            return X + "--" + Y;
        }

        public override bool Equals(object obj)
        {
            Point p = (Point)obj;
            return (this.X == p.X) && (this.Y == p.Y);
        }

        public override int GetHashCode() // key for Dictionary
        {
            int hashX = X.GetHashCode();
            int hashY = Y.GetHashCode();
            return hashX * 10 + hashY * 10;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            Point pt1 = new Point() { X = 10, Y = 10 };
            Point pt2 = new Point() { X = 11, Y = 10 };

            Console.WriteLine(pt1);
            //Console.WriteLine(pt.ToString());
            Console.WriteLine(pt1.Equals(pt2)); //checks if they are referring to same object
            Console.WriteLine(pt1.GetHashCode() + "--" + pt2.GetHashCode());
            Type t1 = pt1.GetType();
            Type t2 = pt2.GetType();
            Console.WriteLine(t1 == t2);
            //Console.WriteLine(pt1.GetType() + "--" + pt2.GetType());

        }
    }
}
