﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceConcept
{
    interface IFigure
    {
        string Shape { get;}
        int Dimension { get; set; }
        double Area();
        double Perimeter();
    }

    class Circle : IFigure
    {
        public string Shape
        {
            get
            {
                return "Circle";
            }
        }

        public Circle(int radius)
        {
            Dimension = radius;
        }

        public int Dimension { get; set; }

        public double Area()
        {
            return Math.PI * Dimension * Dimension;
        }

        public double Perimeter()
        {
            return 2 * Math.PI * Dimension;
        }
    }

    class Square : IFigure
    {   
        public Square(int side)
        {
            Dimension = side;
        }

        public string Shape
        {
            get
            {
                return "Sqaure";
            }
        }

        public int Dimension { get; set; }
     
        double IFigure.Area()
        {
            return Dimension * Dimension;
        }

        public double Perimeter()
        {
            return 4 * Dimension;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            IFigure fig;
            if (args[0] == "C")
                fig = new Circle(10);
            else
                fig = new Square(10);
            Console.WriteLine("Area of {0} is {1}  ", fig.Shape, fig.Area());
            Console.WriteLine("Perimeter of {0} is {1}", fig.Shape, fig.Perimeter());
        }
    }
}
